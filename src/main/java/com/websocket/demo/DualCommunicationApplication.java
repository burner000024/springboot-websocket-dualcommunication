package com.websocket.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DualCommunicationApplication {

	public static void main(String[] args) {
		SpringApplication.run(DualCommunicationApplication.class, args);
	}

}
